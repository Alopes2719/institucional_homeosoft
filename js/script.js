$(document).ready(function () {

  $(window).scroll(function() { 
    var scroll = $(window).scrollTop();
 
    if (scroll > 50) {
        $('.navbar-custom').addClass('navbar-custom-black');
    } else {
        $('.navbar-custom').removeClass('navbar-custom-black');
    }
  });

  $(window).scroll(function() { 
    var scroll = $(window).scrollTop();
 
    if (scroll > 50) {
        $('.last-li').addClass('navbar-custom-btn');
    } else {
        $('.last-li').removeClass('navbar-custom-btn');
    }
  });

	$("#formularioContato").validate({
            // Define as regras
            rules:{
              campoNome:{
                // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 2
              },
              campoTelefone:{
                // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 2
              },
              campoEmail:{
                // campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
                required: true, email: true
              },
              campoMensagem:{
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 2
              }
            },
            // Define as mensagens de erro para cada regra
            messages:{
              campoNome:{
                required: "Digite o seu nome",
                minlength: "O seu nome deve conter, no mínimo, 2 caracteres"
              },
              campoTelefone:{
                required: "Digite o seu telefone",
                minlength: "O seu nome deve conter, no mínimo, 2 caracteres"
              },
              campoEmail:{
                required: "Digite o seu e-mail para contato",
                email: "Digite um e-mail válido"
              },
              campoMensagem:{
                required: "Digite a sua mensagem",
                minlength: "A sua mensagem deve conter, no mínimo, 2 caracteres"
              }
            }
          });

  $("#formularioNews").validate({
            // Define as regras
            rules:{
              campoNome:{
                // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 2
              },
              campoTelefone:{
                // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 2
              },
              campoEmail:{
                // campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
                required: true, email: true
              },
              campoMensagem:{
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 2
              }
            },
            // Define as mensagens de erro para cada regra
            messages:{
              campoNome:{
                required: "Digite o seu nome",
                minlength: "O seu nome deve conter, no mínimo, 2 caracteres"
              },
              campoTelefone:{
                required: "Digite o seu telefone",
                minlength: "O seu nome deve conter, no mínimo, 2 caracteres"
              },
              campoEmail:{
                required: "Digite o seu e-mail",
                email: "Digite um e-mail válido"
              },
              campoMensagem:{
                required: "Digite a sua mensagem",
                minlength: "A sua mensagem deve conter, no mínimo, 2 caracteres"
              }
            }
          });
















});